#!/bin/bash
directory="images"

registry_host="localhost:5000"

if [ ! -d "$directory" ]; then echo "Directory not found: $directory"; exit 1; fi

for file in "$directory"/*.tar.gz; do
  docker load -i "$file"
  image_name=$(basename "$file" .tar.gz)
  new_image_name="$registry_host/$(basename "$image_name")"
  docker tag "$image_name" "$new_image_name"
  docker push "$new_image_name"
done

curl http://localhost:5000/v2/_catalog