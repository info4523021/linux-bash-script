#!/bin/bash 

[ -z "$1" ] && echo "Usage: ./download_images.sh <input_file>" && exit 1

input_file=$1

[ ! -f "$input_file" ] && echo "File not found: $input_file" && exit 1

output_dir="images"
mkdir -p "$output_dir"

while IFS= read -r image_name || [[ -n "$image_name" ]]; do
  echo ""
  if docker pull "$image_name"; then docker save "$image_name" | gzip > "$output_dir/$image_name.tar.gz"; fi
done < <(sed '/^\s*$/d' "$input_file")
